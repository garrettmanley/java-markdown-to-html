# v1.0.0 Testing Plan

## Features to test

### Headers

- TEST 1: H1 through H6

#### Files

- v1.0.0_test_1.md

### Emphasis

- TEST 2: Italics
- TEST 3: Bold
- TEST 4: Bold and Italic

#### Files

- v1.0.0_test_2.md
- v1.0.0_test_3.md
- v1.0.0_test_4.md

### Comprehensive Test File

- v1.0.0_test_5.md