package jmth.file;

import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.List;

public class Parser {
	// Lists
	private static List<String> mdLines = new LinkedList<String>();
	private static List<String> htmlHead = new LinkedList<String>();
	private static List<String> htmlBody = new LinkedList<String>();
	private static List<String> htmlFoot = new LinkedList<String>();

	public static void parseFile(File file) {
		System.out.println("[INFO] Parsing file: " + file.getName() + "...");
		setLines(file);
		mdToHtmlConvert();
		setHtmlHead();
		setHtmlFoot();
		htmlJoinLists();
	}

	private static void mdToHtmlConvert() {
		// Read in lines from mdLines
		for (String line : mdLines) {
			String htmlLine = "";
			char[] chars = line.toCharArray();
			// Check for blank Line
			if (isBlank(line)) {
				htmlBody.add(htmlLine);
			}
			// Check for headers
			else if (isHeader(line)) {
				htmlLine += parseHeader(line, htmlLine);
				htmlBody.add(htmlLine);
			}
			// Check plain text
			else {
				htmlLine += "<p>";
				htmlLine += parseEmphasis(chars, 0);
				htmlLine += "</p>";
				htmlBody.add(htmlLine);
			}

		}
	}

	private static String parseEmphasis(char[] chars, int j) {
		String htmlLine = "";
		boolean isBold = false;
		boolean isItalic = false;
		for (int i = j; i < chars.length; i++) {
			if (chars[i] == '*' && !isItalic) {
				htmlLine += "<i>";
				isItalic = true;
			} else if (chars[i] == '*' && isItalic) {
				htmlLine += "</i>";
				isItalic = false;
			} else if (chars[i] == '_' && !isBold) {
				htmlLine += "<b>";
				isBold = true;
			} else if (chars[i] == '_' && isBold) {
				htmlLine += "</b>";
				isBold = false;
			} else {
				htmlLine += chars[i];
			}
		}
		if (isItalic)
			htmlLine += "</i>";
		if (isBold)
			htmlLine += "</b>";
		return htmlLine;
	}

	// TODO: Pull out function of counting '#'s to a separate method.
	private static String parseHeader(String line, String htmlLine) {
		char[] chars = line.toCharArray();
		// H6
		if (chars[0] == '#' && chars[1] == '#' && chars[2] == '#' && chars[3] == '#' && chars[4] == '#'
				&& chars[5] == '#' && chars[6] == ' ') {
			htmlLine += "<h6>";
			htmlLine += parseEmphasis(chars, 7);
			htmlLine += "</h6>";
			return htmlLine;
		} else if (chars[0] == '#' && chars[1] == '#' && chars[2] == '#' && chars[3] == '#' && chars[4] == '#'
				&& chars[5] == ' ') {
			htmlLine += "<h5>";
			htmlLine += parseEmphasis(chars, 6);
			htmlLine += "</h5>";
			return htmlLine;
		} else if (chars[0] == '#' && chars[1] == '#' && chars[2] == '#' && chars[3] == '#' && chars[4] == ' ') {
			htmlLine += "<h4>";
			htmlLine += parseEmphasis(chars, 5);
			htmlLine += "</h4>";
			return htmlLine;
		} else if (chars[0] == '#' && chars[1] == '#' && chars[2] == '#' && chars[3] == ' ') {
			htmlLine += "<h3>";
			htmlLine += parseEmphasis(chars, 4);
			htmlLine += "</h3>";
			return htmlLine;
		} else if (chars[0] == '#' && chars[1] == '#' && chars[2] == ' ') {
			htmlLine += "<h2>";
			htmlLine += parseEmphasis(chars, 3);
			htmlLine += "</h2>";
			return htmlLine;
		} else {
			htmlLine += "<h1>";
			htmlLine += parseEmphasis(chars, 2);
			htmlLine += "</h1>";
			htmlLine += "\n<hr />";
			return htmlLine;
		}
	}

	private static boolean isBlank(String line) {
		return (line.length() == 0);
	}

	private static boolean isHeader(String line) {
		char[] chars = line.toCharArray();
		if (chars[0] == '#' && chars.length > 0) {
			return true;
		} else {
			return false;
		}
	}

	private static void htmlJoinLists() {
		htmlHead.addAll(htmlBody);
		htmlHead.addAll(htmlFoot);
	}

	private static void setLines(File file) {
		try {
			mdLines = Files.readAllLines(file.getFilePath());
			System.out.println("[SUCCESS] Markdown lines read into parser.");
		} catch (IOException e) {
			System.err.println("[ERROR] Failed to read lines into parser.");
			e.printStackTrace();
		}
	}

	public static List<String> getLines() {
		System.out.println("[INFO] Lines sent to output.");
		return htmlHead;
	}

	private static void setHtmlHead() {
		htmlHead.add("<!DOCTYPE html>");
		htmlHead.add("<html lang='en'>");
		htmlHead.add("\t<head>");
		htmlHead.add("\t<title>Hello, World!</title>");
		htmlHead.add("\t<!-- Required meta tags -->");
		htmlHead.add("\t<meta charset='utf-8'>");
		htmlHead.add("\t<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>");
		htmlHead.add("");
		htmlHead.add("\t<!-- Bootstrap CSS -->");
		htmlHead.add(
				"\t<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css' integrity='sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb' crossorigin='anonymous'>");
		htmlHead.add("</head>");
		htmlHead.add("<body>");
		htmlHead.add("<div class='container'>");
		htmlHead.add("<div class='row'>");
		htmlHead.add("<div class='col-12'>");
	}

	private static void setHtmlFoot() {
		htmlFoot.add("</div>");
		htmlFoot.add("</div>");
		htmlFoot.add("</div>");
		htmlFoot.add("<!-- JS -->");
		htmlFoot.add(
				"<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js' integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN' crossorigin='anonymous'></script>");
		htmlFoot.add(
				"<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js' integrity='sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh' crossorigin='anonymous'></script>");
		htmlFoot.add(
				"<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js' integrity='sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ' crossorigin='anonymous'></script>");
		htmlFoot.add("</body>");
		htmlFoot.add("</html>");

	}

}
