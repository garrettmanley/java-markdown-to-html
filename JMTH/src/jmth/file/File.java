package jmth.file;
import java.nio.file.Path;
import java.nio.file.Paths;

public class File {

	private final Path filePath;
	
	public File(String path) {
		this.filePath = Paths.get(path);
	}

	public Path getFilePath() {
		return filePath;
	}

	public String getName() {
		return getFilePath().getFileName().toString();
	}
	
	public Boolean isMarkdown() {
		return(getFileExtension().equals(".md") || getFileExtension().equals(".MD"));
	}

	public Boolean isHtml() {
		return(getFileExtension().equals(".html"));
	}
	
	private String getFileExtension() {
		String file = getFilePath().getFileName().toString();
		String ext = file.substring(file.lastIndexOf('.'));
		return ext;
	}
	
}
