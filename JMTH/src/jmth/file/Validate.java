package jmth.file;

import java.nio.file.Files;
import jmth.Process;

public class Validate {

	public static void existance(File file, String type) {
		switch (type) {
		case "md":
			if (Files.exists(file.getFilePath())) {
				System.out.println("[SUCCESS] The file exists.");
			} else {
				System.err.println("[ERROR] The file does not exist.");
				Process.terminate();
			}
			break;
		case "html":
			if (Files.exists(file.getFilePath().getParent())) {
				System.out.println(
						"[SUCCESS] The specified directory " + file.getFilePath().getParent().toString() + " exists");
			} else {
				System.err.println("[ERROR] The specified directory " + file.getFilePath().getParent().toString()
						+ " does not exist");
				Process.terminate();
			}
		default:
			break;
		}
	}

	public static void fileType(File file, String type) {
		switch (type) {
		case "md":
			if (file.isMarkdown()) {
				System.out.println("[SUCCESS] The file is a Markdown file.");
			} else {
				System.err.println("[ERROR] The file is not a Markdown file.");
				Process.terminate();
			}
			break;
		case "html":
			if (file.isHtml()) {
				System.out.println("[SUCCESS] The file is a HTML file.");
			} else {
				System.err.println("[ERROR] The file is not a HTML file.");
				Process.terminate();
			}
			break;
		default:
			System.err.println("[FAIL] Invalid file type validation option chosen");
			Process.terminate();
			break;
		}
	}

	public static void readability(File file) {
		if (Files.isReadable(file.getFilePath())) {
			System.out.println("[SUCCESS] The file can be read.");
		} else {
			System.err.println("[ERROR] The file cannot be read.");
			Process.terminate();
		}
	}

}
