package jmth.io;
import java.io.IOException;
import java.nio.file.Files;

import jmth.file.File;
import jmth.file.Parser;

public class Output {
	public static void saveFile(File file) {
		//Create new file
		File htmlFile = new File(file.getFilePath().toString() + ".html");
		//Save File
		try {
			Files.write(htmlFile.getFilePath(), Parser.getLines());
			System.out.println("[SUCCESS] " + htmlFile.getName() + " saved.");
		} catch (IOException e) {
			System.err.println("[ERROR] Failed to save file " + htmlFile.getName());
			e.printStackTrace();
		}
	}
	public static void saveFile(File mdFile, File htmlFile) {
		try {
			Files.write(htmlFile.getFilePath(), Parser.getLines());
			System.out.println("[SUCCESS] " + htmlFile.getName() + " saved.");
		} catch (IOException e) {
			System.err.println("[ERROR] Failed to save file " + htmlFile.getName());
			e.printStackTrace();
		}
	}
}
