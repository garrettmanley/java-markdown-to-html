package jmth.io;

import jmth.file.File;
import jmth.file.Validate;

//Fetches the Markdown file from the system.
public class Input {

	/*
	 * Takes in the newly generated File Object and processes it before sending it
	 * to the parser to be converted to HTML
	 */
	public static void fetchFile(File file, String type) {
		validateFile(file, type);
	}

	private static void validateFile(File file, String type) {
		System.out.println("[INFO] Validating file: " + file.getName() + " for type: " + type);
		if (type.equals("md")) {
			Validate.existance(file, "md");
			Validate.fileType(file, type);
			Validate.readability(file);
		} else if (type.equals("html")) {
			Validate.fileType(file, type);
			Validate.existance(file, "html");
		}

	}
}
