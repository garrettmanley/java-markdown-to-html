package jmth;

import java.io.IOException;

import jmth.file.File;
import jmth.file.Parser;
import jmth.io.Input;
import jmth.io.Output;

public class run {

	public static void main(String[] args) throws IOException {
		File file;
		// determines the arguments on the program call
		switch (args.length) {
		case 0:
			System.err.println("[ERROR] No markup file specified.");
			System.out.println("[INFO] Specify the name of file in the directory that you would like to parse.");
			Process.terminate();
			// -----------------------------------
			// File Saved to current directory
			// -----------------------------------
		case 1:
			System.out.println("[INFO] Checking for file: " + args[0]);
			// Create File Object
			file = new File(args[0]);
			Input.fetchFile(file, "md");
			Parser.parseFile(file);
			Output.saveFile(file);
			Process.terminate();
			// -----------------------------------
			// File saved to specified directory
			// -----------------------------------
		case 2:
			System.out.println("[INFO] Checking for file: " + args[0] + " to save as file: " + args[1]);
			file = new File(args[0]);
			File htmlFile = new File(args[1]);
			Input.fetchFile(file, "md");
			Input.fetchFile(htmlFile, "html");
			Parser.parseFile(file);
			Output.saveFile(file, htmlFile);
			Process.terminate();
		default:
			System.err.println("[ERROR] Too many inputs.");
			System.out.println("[INFO] Please only specify the path of the markdown that you want parsed.");

		}
	}
}
