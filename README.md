# Java - Markdown to HTML

## Summary of Markdown

These are the markdown features that are to be implemented by the project. for Version 1.0 of JMTH. Note that these are not all of the features that are currently available in Markdown.

### Headers

```
Defined by #
H1 = #
H2 = ##
H3 = ###
...
```

### Emphasis

#### Italics

```
*wrapping* a string with single asterisks applies italics.
```

Example: *wrapping*

#### Bold

```
_wrapping_ a string with underlines applies bold.
```

Example: __wrapping__

## Special Characters
```
#
*
_
```